import torch
import sys
from model import TransformerModel
import dataset as ds

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Charger le dataset
dataset = ds.CharDataset(64, open('shakespeare.txt', 'r').read())

embed_size = 768
num_heads = 8
num_layers = 2
dropout = 0.1

# Load the trained model
model = TransformerModel(
    vocab_size=dataset.get_vocab_size(),
	embed_size=embed_size,
	num_heads=num_heads,
	num_layers=num_layers,
	dropout=dropout,
).to(device)

model.load_state_dict(torch.load(sys.argv[1]))  # Load model weights
model.eval()

def generate_text(model, start_sequence, max_length=50):
    sequence = [dataset.stoi[char] for char in start_sequence]
    for _ in range(max_length):
        input_tensor = torch.tensor([sequence], dtype=torch.long).to(device)
        with torch.no_grad():
            output_logits = model(input_tensor)
        # Use the logits from the last position to predict the next character
        probabilities = torch.softmax(output_logits[0, -1, :], dim=0).to(device)
        next_char_idx = torch.multinomial(probabilities, 1).item()
        sequence.append(next_char_idx)
    return ''.join([dataset.itos[idx] for idx in sequence])

# Example usage
start_sequence = "O God, O God!"
generated_text = generate_text(model, start_sequence)
print(generated_text)

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, random_split
import wandb
from model import TransformerModel
import dataset as ds

wandb.login(key="22f2732ae673416ad5765576305fee6bde2e6e13")

run = wandb.init(project="lslm", entity="noustous")

config = wandb.config
config.learning_rate = 0.001
config.epochs = 20
config.batch_size = 128

config.embed_size = 768
config.num_heads = 8
config.num_layers = 12
config.dropout = 0.5

config.step_size = 2
config.gamma = 0.8

config.block_size = 128

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

dataset = ds.CharDataset(wandb.config.block_size, open("shakespeare.txt", "r").read())

train_size = int(0.7 * len(dataset))  # 70% pour l'entraînement
val_size = int(0.15 * len(dataset))  # 15% pour la validation
test_size = len(dataset) - train_size - val_size  # 15% pour le test

train_dataset, val_dataset, test_dataset = random_split(
    dataset, [train_size, val_size, test_size]
)

train_dataloader = DataLoader(
    train_dataset, batch_size=wandb.config.batch_size, shuffle=True
)
val_dataloader = DataLoader(
    val_dataset, batch_size=wandb.config.batch_size, shuffle=True
)
test_dataloader = DataLoader(
    test_dataset, batch_size=wandb.config.batch_size, shuffle=True
)

config.vocab_size = dataset.get_vocab_size()

model = TransformerModel(
    vocab_size=config.vocab_size,
    sequence_length=config.block_size,
    embed_size=config.embed_size,
    num_heads=config.num_heads,
    num_layers=config.num_layers,
    dropout=config.dropout,
).to(device)

criterion = nn.CrossEntropyLoss().to(device)
optimiser = optim.AdamW(model.parameters(), lr=config.learning_rate)
scheduler = optim.lr_scheduler.StepLR(
    optimiser, step_size=config.step_size, gamma=config.gamma
)

def train(
    model: nn.Module,
    dataloader: DataLoader,
    optimizer: optim.Optimizer,
    criterion: nn.Module,
):
    model.train()
    total_loss: float = 0.0
    for input_seq, target_seq in dataloader:
        input_seq, target_seq = input_seq.to(device), target_seq.to(device)

        optimizer.zero_grad()
        output = model(input_seq)
        loss = criterion(output.view(-1, len(dataset.stoi)), target_seq.view(-1))
        loss.backward()
        optimizer.step()

        total_loss += loss.item()
        wandb.log({"batch_loss": loss.item()})

    average_loss: float = total_loss / len(dataloader)
    return average_loss


def validate(model: nn.Module, dataloader: DataLoader, criterion: nn.Module):
    model.eval()
    total_loss = 0.0
    with torch.no_grad():
        for input_seq, target_seq in dataloader:
            input_seq, target_seq = input_seq.to(device), target_seq.to(device)
            output = model(input_seq)
            loss = criterion(output.view(-1, len(dataset.stoi)), target_seq.view(-1))
            total_loss += loss.item()
    return total_loss / len(dataloader)


def evaluate(model: nn.Module, dataloader: DataLoader, criterion: nn.Module):
    model.eval()
    total_loss = 0.0
    correct_predictions = 0
    total_predictions = 0

    with torch.no_grad():
        for input_seq, target_seq in dataloader:
            input_seq, target_seq = input_seq.to(device), target_seq.to(device)
            output = model(input_seq)

            loss = criterion(output.view(-1, config.vocab_size), target_seq.view(-1))
            total_loss += loss.item()

            _, predicted = torch.max(output, -1)
            correct_predictions += (predicted == target_seq).sum().item()
            total_predictions += target_seq.numel()

    average_loss = total_loss / len(dataloader)
    accuracy = correct_predictions / total_predictions
    return average_loss, accuracy


def save_checkpoint(
    model: nn.Module,
    optimizer: optim.Optimizer,
    filename: str = "my_checkpoint.pth.tar",
):
    print("=> Saving checkpoint")
    checkpoint = {
        "state_dict": model.state_dict(),
        "optimizer": optimizer.state_dict(),
    }
    torch.save(checkpoint, filename)


for epoch in range(config.epochs):
    epoch_loss = train(model, train_dataloader, optimiser, criterion)
    val_loss = validate(model, val_dataloader, criterion)
    wandb.log({"epoch_loss": epoch_loss, "val_loss": val_loss, "epoch": epoch})
    scheduler.step()

    if epoch % 5 == 0:
        save_checkpoint(model, optimiser, filename=f"checkpoint_epoch_{epoch}.pth.tar")

torch.save(model.state_dict(), f"model_{run.name}.pt")

test_loss, test_accuracy = evaluate(model, test_dataloader, criterion)
wandb.log({"test_loss": test_loss, "test_accuracy": test_accuracy})
print(f"Test Loss: {test_loss:.4f}, Test Accuracy: {test_accuracy:.4f}")

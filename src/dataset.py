import torch
from torch.utils.data import Dataset


class CharDataset(Dataset):
	"""
	Character dataset for Large Small Language Model

	Args:
		block_size: block size
		data: text data

	Attributes:
		data: text data
		stoi: mapping from characters to integers
		itos: mapping from integers to characters
		block_size: block size
		voc_size: vocabulary size
		data_size: size of text data
		idx_data: text data converted to integers
		overlap: whether to overlap input sequences or not

	Methods:
		get_vocab_size: returns vocabulary size
		__len__: returns length of dataset
		__getitem__: returns input and target sequences
	"""

	__slots__ = ['data', 'stoi', 'itos', 'block_size', 'voc_size', 'data_size', 'idx_data']

	def __init__(self, block_size: int, data: str, overlap: bool = True):
		# TOASK: Convert data to lowercase
		# data = data.lower()

		self.data = data  # .lower()

		# Get all unique characters and sort them
		chars = sorted(list(set(data)))

		# Create a mapping from characters to integers and vice versa
		self.stoi = {c: i for i, c in enumerate(chars)}
		self.itos = {i: c for i, c in enumerate(chars)}

		self.block_size = block_size
		self.voc_size = len(chars)
		self.data_size = len(data)

		# Convert data to integers
		self.idx_data = [self.stoi[c] for c in data]

		self.overlap = overlap

	def get_vocab_size(self):
		"""Returns vocabulary size"""
		return self.voc_size

	def __len__(self):
		"""Returns length of dataset"""
		if self.overlap:
			return self.data_size - self.block_size
		else:
			return (len(self.idx_data) + self.block_size - 1) // self.block_size

	def __getitem__(self, idx):
		"""Returns input and target sequences"""

		# Get a chunk of data of size block_size + 1
		if self.overlap:
			start_idx = idx
			end_idx = idx + self.block_size + 1
		else:
			start_idx = idx * self.block_size
			end_idx = (idx + 1) * self.block_size + 1

		chunk = self.idx_data[start_idx:end_idx]
		chunk += [self.stoi[' ']] * (self.block_size + 1 - len(chunk))  # Padding with spaces if chunk is short

		# Create input as 0 to block_size-1 and target as 1 to block_size
		input_seq = torch.tensor(chunk[:-1], dtype=torch.long)
		target_seq = torch.tensor(chunk[1:], dtype=torch.long)

		return input_seq, target_seq


if __name__ == '__main__':
	demo = CharDataset(3, 'Hello, World!')
	print(f'Text: {demo.data}')
	print(f'Vocabulary size: {demo.get_vocab_size()}')
	print(f'Length of dataset: {len(demo)}')
	print()
	print(f'Dictionary: {demo.stoi}')
	print(f'Inverse dictionary: {demo.itos}')
	print()
	print("With overlap:")
	print(f'First input and target sequences: {demo[0]}')
	print(f'Input sequence as characters: {[demo.itos[int(i)] for i in demo[0][0]]}')
	print(f'Target sequence as characters: {[demo.itos[int(i)] for i in demo[0][1]]}')
	print()
	print(f'Second input and target sequences: {demo[1]}')
	print(f'Input sequence as characters: {[demo.itos[int(i)] for i in demo[1][0]]}')
	print(f'Target sequence as characters: {[demo.itos[int(i)] for i in demo[1][1]]}')
	print()

	demo = CharDataset(3, 'Hello, World!', overlap=False)
	print("Without overlap:")
	print(f'First input and target sequences: {demo[0]}')
	print(f'Input sequence as characters: {[demo.itos[int(i)] for i in demo[0][0]]}')
	print(f'Target sequence as characters: {[demo.itos[int(i)] for i in demo[0][1]]}')
	print()
	print(f'Second input and target sequences: {demo[1]}')
	print(f'Input sequence as characters: {[demo.itos[int(i)] for i in demo[1][0]]}')
	print(f'Target sequence as characters: {[demo.itos[int(i)] for i in demo[1][1]]}')

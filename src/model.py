import torch
import torch.nn as nn


class TransformerBlock(nn.Module):
    def __init__(self, embed_size=768, heads=8, dropout=0.01, forward_expansion=4):
        super().__init__()
        self.attention = nn.MultiheadAttention(
            embed_dim=embed_size, num_heads=heads, dropout=dropout, batch_first=True
        )
        self.norm1 = nn.LayerNorm(embed_size)
        self.norm2 = nn.LayerNorm(embed_size)

        self.feed_forward = nn.Sequential(
            nn.Linear(embed_size, forward_expansion * embed_size),
            nn.ReLU(),
            nn.Linear(forward_expansion * embed_size, embed_size),
        )

        self.dropout = nn.Dropout(dropout)

    def forward(self, value, key, query, mask):
        if mask is not None:
            attention = self.attention(query, key, value, attn_mask=mask)[0]
        else:
            attention = self.attention(query, key, value)[0]
            
        x = self.dropout(self.norm1(attention + query))
        forward = self.feed_forward(x)
        out = self.dropout(self.norm2(forward + x))
        return out


class TransformerModel(nn.Module):
    def __init__(
        self, vocab_size, sequence_length, embed_size=768, num_heads=8, num_layers=12, dropout=0.1
    ):
        super(TransformerModel, self).__init__()
        self.num_heads = num_heads
        self.embedding = nn.Embedding(vocab_size, embed_size)
        self.pos_encoder = nn.Embedding(sequence_length, embed_size)

        self.transformer_blocks = nn.ModuleList(
            [
                TransformerBlock(embed_size, num_heads, dropout, forward_expansion=4)
                for _ in range(num_layers)
            ]
        )
        self.fc_out = nn.Linear(embed_size, vocab_size)

    def forward(self, x):
        batch_size, seq_length = x.size(0), x.size(1)
        positions = torch.arange(0, seq_length).unsqueeze(0).to(x.device)
        x = self.embedding(x) + self.pos_encoder(positions)
        mask = self.generate_square_subsequent_mask(seq_length).to(x.device)
        mask = mask.expand(batch_size*self.num_heads, -1, -1)
        for transformer_block in self.transformer_blocks:
            x = transformer_block(x, x, x, mask=mask)

        x = self.fc_out(x)
        return x
    
    def generate_square_subsequent_mask(self, sz_seq):
        mask = (torch.triu(torch.ones(sz_seq, sz_seq)) == 1).transpose(0, 1)
        mask = (
            mask.float()
            .masked_fill(mask == 0, float("-inf"))
            .masked_fill(mask == 1, float(0.0))
        )
        return mask

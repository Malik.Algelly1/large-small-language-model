# Large Small Language Model - Shakespeare Text Generation

This project implements a transformer-based, character-level language model similar to GPT, trained on the complete works of William Shakespeare. The model is capable of generating Shakespearean-like text, providing an interesting perspective on how deep learning can be applied to understand and emulate classical literature styles.

## Objective

The goal is to train a transformer-based model to predict the next character in a sequence, enabling it to generate Shakespearean-like text from a given seed string.

## Dataset

The model is trained on the Shakespeare dataset, which includes his plays, poems, and sonnets. The dataset can be downloaded from the following link: 
[Download Shakespeare Dataset](https://raw.githubusercontent.com/karpathy/char-rnn/master/data/tinyshakespeare/input.txt)

Each character in the input data is mapped to an index from a dictionary, and the input to the model is in the form (B, N), where B is the batch size and N is the number of tokens in each sequence.

## Model Architecture

The model follows the Transformer's decoder-only architecture with the following elements:

- Input token embeddings
- Causal multi-head self-attention mechanism
- Positional encodings
- Feed-forward neural networks
- Residual connections
- Layer normalizations

The model was tested with 12 layers, 8 attention heads, and 768 embedding dimensions.
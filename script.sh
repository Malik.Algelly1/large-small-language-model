#!/bin/sh
#SBATCH --job-name=Shake
#SBATCH --output=Shake-%J.out

#SBATCH --partition=shared-gpu

#SBATCH --ntasks=1
#SBATCH --ntasks-per-node=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=4
#SBATCH --gpus=4

#SBATCH --time=10:00:00


module load foss
module load Python
module load SciPy-bundle
module load GCC/11.3.0 OpenMPI/4.1.4
module load PyTorch h5py/3.7.0
module load scikit-learn/1.2.2
module load Seaborn/0.12.1
module load wandb/0.13.6

python -u src/training.py
